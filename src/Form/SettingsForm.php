<?php

namespace Drupal\nodemetalist\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\NodeType;

/**
 * Configure Node Meta list settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'nodemetalist.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'node_meta_list_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['nodemetalist.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = \Drupal::config('nodemetalist.settings');
    $default_values = $config->get('node_meta_entities');

    $all_content_types = NodeType::loadMultiple();
    foreach ($all_content_types as $machine_name => $content_type) {
      $content_type_list[$machine_name] = $content_type->label();
    }

    $form['settings']['node_meta_entities'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Node Meta Enabled Entities'),
      '#description' => $this->t('Select the entities that printable support should be enabled for.'),
      '#options' => [],
      '#default_value' => [],
    ];

    foreach ($content_type_list as $key => $value) {
      $form['settings']['node_meta_entities']['#options'][$key] = $value;
    }
    if(!empty($default_values)){
    foreach ($default_values as $key => $value) {
      $form['settings']['node_meta_entities']['#default_value'][] = $value;
    }
  }
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $this->configFactory->getEditable('nodemetalist.settings')
      ->set('node_meta_entities', $form_state->getValue('node_meta_entities'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
