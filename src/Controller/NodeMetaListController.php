<?php

namespace Drupal\nodemetalist\Controller;

use Drupal\node\Entity\Node;
use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for Node Meta list routes.
 */
class NodeMetaListController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build() {
    $header = [
      'node_title' => t('Node Title'),
      'content_type' => t('Content Type'),
      'meta_title' => t('Meta Title'),
      'meta_description' => t('Meta Description'),
      'met_keywords' => t('Meta Keywords'),
    ];

    $config = \Drupal::config('nodemetalist.settings');
    $content_types = $config->get('node_meta_entities');

    $query = \Drupal::entityQuery('node')
      ->condition('type', array_values($content_types), 'IN')
      ->accessCheck(TRUE);
    $results = $query->execute();
    $final_data = [];
    $nodes = Node::loadMultiple($results);
    foreach ($nodes as $key => $node) {
      $metatag_manager = \Drupal::service('metatag.manager');
      $tags = $metatag_manager->tagsFromEntity($node);
      $final_data[$key]['title'] = $node->getTitle();
      $final_data[$key]['bundle'] = $node->bundle();
      if (!empty($tags['title'])) {
        $final_data[$key]['meta_title'] = $tags['title'];
      }

      if (!empty($tags['description'])) {
        $final_data[$key]['meta_description'] = $tags['description'];
      }

      if (!empty($tags['keywords'])) {
        $final_data[$key]['meta_keywords'] = $tags['keywords'];
      }
    }

    $build['tables'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $final_data,
      '#empty' => t('No data found'),

    ];
    $build['pager'] = [
      '#type' => 'pager',
    ];
    return $build;
  }

}
