# CONTENTS OF THIS FILE

 - Introduction
 - Requirements
 - Features
 - Standard usage scenario
 - Maintainers

## Introduction

This module listing the Meta information in table format. It presents the all Node's Meta information like MetaTitle, Meta Description and Meta Keywords for all  pages.

We are using metatag module as parent and capturing the information in table format.

For additional information, see the [online documentation](https://www.drupal.org/docs/8/modules/metatag).

This version should work with all Drupal 9 releases, though it
is always recommended to keep Drupal core installations up to date.

## Requirements

Node Meta List for Drupal 9 requires the following:

 - [Metatag](https://www.drupal.org/project/metatag): Provides Meta inforation to nodes.

## Features

The primary features include:

 - An administration interface to manage selecting the content  types for show the meta tags.



## Standard usage scenario

 - Install the module.
 - Open `admin/config/search/metatag/settings/node-meta-list`.
 - Select the Content type to see the meta information and save it.
 - Then visit Open `admin/config/search/metatag/node-meta-list/data`.
 - You will be able to see the Meta information in table format.

### To Do

 - Pagination in table format.

## Maintainers

Current Maintainers:

 - [phpsubbarao](https://www.drupal.org/u/phpsubbarao)
